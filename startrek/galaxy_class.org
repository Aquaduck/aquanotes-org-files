#+title: Galaxy-class
#+date: [2021-07-07 Wed]
* Overview
- The *Galaxy*-class was a [[file:starfleet.org][Starfleet]] vessel first introduced in the mid 2360s
- It was one of the largest and most powerful [[file:United Federations of Planets.org][Federation]] starship classes of its time
