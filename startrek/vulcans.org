#+title: Vulcans
#+date: [2021-07-07 Wed]
* Overview
- The *Vulcans*, or *Vulcanians*, were a warp-capable humanoid species from the planet Vulcan. They were widely renowned for their strict adherence to logic and reason as well as their remarkable stoicism
- In 2161, their homeworld became a founding member of the [[file:United Federations of Planets.org][United Federations of Planets]]
