#+title: Haldane effect
#+date: [2021-10-27 Wed]
#+hugo_tags: medschool concept

* Concept links
- The [[file:carbon_dioxide.org][carbon dioxide]] affinity of [[file:hemoglobin.org][Hemoglobin]] is *inversely proportional* to the [[file:oxygen.org][oxygenation]] of hemoglobin
