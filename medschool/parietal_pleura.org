#+title: Parietal pleura
#+date: [2021-10-16 Sat]
#+hugo_tags: medschool concept

* Concept links
- A type of [[file:pleura.org][Pleura]]
- The outer layer that connects to the thoracic wall, the [[file:mediastinum.org][mediastinum]], and the [[file:diaphragm.org][diaphragm]]
