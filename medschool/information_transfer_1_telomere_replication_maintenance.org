#+title: Information Transfer 1: Telomere Replication & Maintenance
#+date: [2021-08-21 Sat]
#+hugo_tags: medschool source
* DNA Replication in Eukaryotes
- Human genome has 3 billion base pairs per haploid set of chromosomes
  + 6 billion replicated during [[file:s_phase.org][S phase]] of the [[file:cell_cycle.org][cell cycle]]
- *Multiple origins of replication on each eukaryotic chromosome*
- Rate of replication ~ *100 nucleotides/s*
  + Much *slower than prokaryotic replication*
- *14 [[file:dna_polymerase.org][DNA polymerases]] in [[file:eukaryote.org][eukaryotes]]*
  + 5 are known to have major roles during replication and have been well studied:
    - [[file:dna_polymerase_ɑ.org][pol α]] (alpha)
    - pol β (beta)
    - pol γ (gamma)
    - [[file:dna_polymerase_δ.org][pol ẟ]] (delta)
    - [[file:dna_polymerase_ɛ.org][pol ε]] (epsilon)
** Steps in replication
The essential steps of replication are the same as in prokaryotes
1) Before replication begins, DNA has to be made available as a template
   + [[file:dna.org][DNA]] is bound to basic proteins known as [[file:histone.org][histones]] to form stuctures called [[file:nucleosome.org][nucleosomes]]
   + [[file:histone.org][Histones]] must be removed and then replaced during the replication process -> accounts for lower replication rate in [[file:eukaryote.org][eukaryotes]]
   + [[file:chromatin.org][Chromatin]] undergoes chemical modifications so that DNA can slide off histones or be accessible to the enzymes of the DNA replication machinery
   + At the origin of replication, a pre-replication complex is made with other initiator proteins
     - [[file:helicase.org][Helicase]] and other proteins are then recruited to start replication
2) A [[file:helicase.org][helicase]] opens up the DNA helix using energy from ATP hydrolysis
   + Replication forks are formed at each replication origin as the DNA unwinds
     - This causes overwinding/supercoiling
       + Resolved by the action of [[file:topoisomerase.org][topoisomerases]]
3) Primers are formed by the enzyme [[file:primase.org][primase]]
   + Using the primer, [[file:dna_polymerase.org][DNA polymerase]] can start synthesis
4) DNA pol α adds a short (20-30 nucleotide) DNA fragment to the RNA primer on both strands, and then hands off to a second polymerase (DNA Pol-δ or DNA Pol-ε)
   + [[file:dna_polymerase_δ.org][DNA Pol-δ]]  synthesizes lagging strand
   + [[file:dna_polymerase_ɛ.org][DNA Pol-ε]]  synthesizes leading strand
5) A sliding clamp protein known as [[file:proliferating_cell_nuclear_antigen.org][PCNA]] (proliferating cell nuclear antigen) holds the DNA polymerase in place so that it does not slide off DNA
6) As pol δ runs into the primer RNA on the lagging strand, it displaces it from the DNA template
   + Then removed by [[file:rnase_h.org][RNase H]] (aka flap endonuclease) and replaced with DNA nucleotides
   + Gaps between okazaki fragments sealed by [[file:dna_ligase.org][DNA ligase]], which forms the phosphodiester bond
** Differences between Prokaryotic and Eukaryotic replication
| Property              | Prokaryotes        | Eukaryotes           |
|-----------------------+--------------------+----------------------|
| Origin of replication | Single             | Multiple             |
| Rate of replication   | 1000 nucleotides/s | 50-100 nucleotides/s |
| DNA polymerase types  | 5                  | 14                   |
| Telomerase            | Not present        | Present              |
| RNA primer removal    | DNA pol I          | RNase H              |
| Strand elongation     | DNA pol III        | Pol α, pol δ, pol ε  |
| Sliding clamp         | Sliding clamp      | PCNA                 |
** [[file:telomere.org][Telomere]] replication
- Unlike prokaryotic chromosomes, eukaryotic chromosomes are linear
  + When the replication fork reaches the end of the linear chromosome, there is *no way* to replace the primer on the 5' end of the lagging strand
    - The DNA at the ends of the chromosome remain unpaired, and over time these get ends get progressively shorter as cells continue to divide
- /Telomeres/ comprise repetitive sequences that code for no gene
  + In humans, this is a six-base-pair sequence TTAGGG which is repeated 100 to 1000 times in the telomere regions
  + This in effect *protects the genes from getting deleted as cells continue to divide*
- [[file:telomerase.org][Telomerase]] is responsible for adding telomeres
  + Contains two parts: a *catalytic part* and a *built-in RNA template*
    - Attaches to end of a chromosome via DNA nucleotides complementary to the RNA template
      + Corresponding nucleotides are added on the 3' end of the DNA strand
      + Once sufficiently elongated, DNA pol adds the complementary nucleotides to the other strand -> chromosomes successfully replicated
  + Telomerase is typically active in germ cells and adult stem cells, and *not* active in adult somatic cells
** [[file:telomerase.org][Telomerase]] and aging
- Cells that undergo cell division continue to have their telomeres shortened because most somatic cels do not make telomerase
  + Means that *telomere shortening is associated with aging*
- In 2010, scientists found that telomerase cna reverse some age-related conditions in mice
  + Telomerase-deficient mice had muscle atrophy, stem cell depletion, organ system failure, and impaired tissue injury responses
  + Following reactivation of telomerase -> caused extension of telomeres, reduced DNA damage, reversed neurodegeneration, and improved function of testes, spleen, and intestines
- [[file:cancer.org][Cancer]] is characterized by uncontrolled cell division of abnormal cells
  + Cells accumulate mutations, proliferate uncontrollably, and can migrate to different parts of the body through a process called [[file:metastasis.org][metastatis]]
  + Scientists have observed that *cancerous cells have considerably shortened telomeres and that telomerase is active in these cells*
    - Only *after telomeres were shortened* did telomerase become active
      + If action of telomerase in these cells can be inhibited -> cancerous cells potentially stopped from further division
