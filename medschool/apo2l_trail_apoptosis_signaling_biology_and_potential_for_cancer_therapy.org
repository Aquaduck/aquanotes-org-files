#+title: Apo2L/TRAIL: apoptosis signaling, biology, and potential for cancer therapy
#+date: [2021-09-27 Mon]
#+hugo_tags: medschool source

* Abstract
- Apo2 ligand or tumor necrosis factor (TNF)-related apoptosis-inducing ligand ([[file:apo2_ligand.org][Apo2L/TRAIL]]) is one of several members of the TNF gene superfamily that induce apoptosis through engagement of death receptors
* Apoptosis signaling by [[file:apo2_ligand.org][Apo2L]]/TRAIL
- Similar to [[file:fas_ligand.org][FasL]], TRAIL initiates apoptosis upon binding to its cognate death receptors by inducing the recruitment of specific cytoplasmic proteins to the intracellular death domain of the receptor -> formation of [[file:death_inducing_signaling_complex.org][DISC]]
