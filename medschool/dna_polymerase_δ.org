#+title: DNA Polymerase-δ
#+date: [2021-08-17 Tue]
#+hugo_tags: medschool concept
#+roam_alias: "DNA Polymerase delta" "Pol-δ"
* Overview
- A type of [[file:dna_polymerase.org][DNA Polymerase]] in [[file:eukaryote.org][eukaryotes]]
- Has *3'-5' exonuclease activity*
