#+title: Define statistical power and explain how it affects the interpretation of negative results
#+date: [2021-10-31 Sun]
#+hugo_tags: medschool learning_objective

* From [[file:amboss.org][Amboss]]
** [[file:statistical_power.org][Statistical power]]
- The probability of correctly rejecting the [[file:null_hypothesis.org][null hypothesis]]
- Complementary to the [[file:type_ii_error.org][type 2 error]] rate
- Positively correlates with sample size and magnitude of association of interest
  + i.e. *increasing sample size = increasing statistical power*
  + Correlates with measurement [[file:accuracy.org][accuracy]]
- Most studies aim to achieve *80% statistical power*
