#+title: Inspiratory vital capacity
#+date: [2021-10-26 Tue]
#+hugo_tags: medschool concept
#+roam_alias: IVC

* Concept links
- Can be a way to measure [[file:vital_capacity.org][VC]]
