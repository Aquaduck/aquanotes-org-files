#+title: Src Homology 2 domain
#+date: [2021-09-05 Sun]
#+hugo_tags: medschool concept
#+roam_alias: "SH2 domain" "SH_{2} domain" "SH2" "SH_{2}"
