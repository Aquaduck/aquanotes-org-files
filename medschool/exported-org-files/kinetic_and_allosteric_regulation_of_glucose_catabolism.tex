% Created 2021-08-31 Tue 11:51
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\author{Arif Ahsan}
\date{\textit{[2021-08-19 Thu]}}
\title{Kinetic and Allosteric Regulation of Glucose Catabolism}
\hypersetup{
 pdfauthor={Arif Ahsan},
 pdftitle={Kinetic and Allosteric Regulation of Glucose Catabolism},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.5)}, 
 pdflang={English}}
\begin{document}

\maketitle
\emph{Adapted from Lippincott Illustrated Reviews: Biochemistry, 7e Chapter 5: Enzymes}
Updated version with more enzymes uploaded for August 30th workshop called ``Enzymes Regulating Intermediary Carbohydrate \& Fat Metabolism''

\section{Allosteric enzymes}
\label{sec:orgd4838d0}
\begin{itemize}
\item Allosteric enzymes are regulated by molecules called effectors
\begin{itemize}
\item Effectors bind \textbf{noncovalently} at a site \textbf{other than the active site}
\end{itemize}
\item Allosteric enzymes are more sensitive in their response to changes in substrate concentrations
\begin{itemize}
\item Display a ``switch-like'' transition from low-to-high reaction rates as ↑[substrate] -> \textbf{sigmoidal curve}
\end{itemize}
\item Almost always composed of multiple subunits
\begin{itemize}
\item Regulatory (allosteric) site distinct from substrate-binding site
\begin{itemize}
\item May be located on a subunit that is not itself catalytic
\end{itemize}
\end{itemize}
\item \emph{Negative effectors}: inhibit enzyme activity
\item \emph{Positive effectors}: increase enzyme activity
\end{itemize}
\subsection{Homotropic effectors}
\label{sec:org5c942e9}
\begin{itemize}
\item When the \textbf{substrate itself is the effector}
\item Most often a \textbf{positive effector}
\begin{itemize}
\item Presence of substrate at one site on the enzyme -> enhances catalytic properties of other substrate-binding sites -> \textbf{Cooperativity}
\end{itemize}
\item Show a \textbf{sigmoidal curve} when V\textsubscript{o} is plotted against [substrate]
\begin{itemize}
\item Contrasts with the hyperbola of enzymes following \href{michaelis\_menten\_kinetics.org}{Michaelis-Menten kinetics}
\end{itemize}
\end{itemize}
\subsection{Heterotrophic effectors}
\label{sec:orgc4438ed}
\begin{itemize}
\item \textbf{Different from substrate}
\item \emph{Feedback inhibition} provides cell w/ appropriate amounts of product needed by \textbf{regulating flow of substrate molecules through pathway that synthesizes that product}
\end{itemize}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{/home/kita/Documents/logseq-org-roam-wiki/hugo/content-org/.attach/94/d953ae-b1f8-4307-91d5-c50de3368073/_20210819_154506screenshot.png}
\caption{Feedback inhibition of a metabolic pathway}
\end{figure}
\section{\href{glycolysis.org}{Glycolysis}}
\label{sec:org20cf49e}
\begin{itemize}
\item Occurs \textbf{primarily in the fed state}
\item All tissues \textbf{except \href{liver.org}{liver}} use glucose via glycolysis as an energy source in the fed state
\begin{itemize}
\item \href{brain.org}{Brain} has \textbf{obligate requirement} for glucose
\item Liver uses glucose to \textbf{initially replenish its glycogen stores}
\begin{itemize}
\item Secondarily will generate acetyl-CoA via glycolysis and pyruvate DH complex for use in cholesterol synthesis
\item Liver \textbf{prefers to use amino acids} provided by dietary protein as energy source in fed state
\end{itemize}
\end{itemize}
\end{itemize}
\subsection{\href{hexokinase.org}{Hexokinases} I-III}
\label{sec:orgfb6efb8}
\begin{itemize}
\item In most tissues (other than pancreas + liver), the first reaction in glycolytic pathway is catalyzed by one of the isozymes of hexokinase
\item Hexokinase catalyzes the \textbf{first} of three highly-regulated \textbf{irreversible} glycolytic reactions
\begin{itemize}
\item Hexokinase catalyzes ATP-dependent phosphorylation of glucose to \href{glucose\_6\_phosphate.org}{G6P}
\begin{itemize}
\item G6P cannot diffuse out of the cell -> allows for continuous glucose uptake by cells
\end{itemize}
\end{itemize}
\item Hexokinase enzymes are \textbf{inhibited by \href{glucose\_6\_phosphate.org}{G6P}, the reaction product}
\begin{itemize}
\item Low K\textsubscript{m} (high affinity) for glucose
\item Low V\textsubscript{max} (low capacity) for glucose phosphorylation
\begin{itemize}
\item Allows for efficient phosphorylation and metabolism of glucose even w/ low tissue concentrations
\end{itemize}
\item This makes them \textbf{allosteric enzymes}
\end{itemize}
\end{itemize}
\subsection{\href{glucokinase.org}{Glucokinase} (hexokinase IV)}
\label{sec:orgb22e0ea}
\begin{itemize}
\item \textbf{Restricted to liver parenchymal cells and β-cells of pancreas}
\item Has cooperativity with [glucose]
\item Has a \textbf{much higher K\textsubscript{m} and V\textsubscript{max}}
\begin{itemize}
\item Low affinity for glucose ensures that when [glucose] is high, it will be trapped in the liver
\begin{itemize}
\item Whereas when [glucose] is low, it \textbf{will not be recognized as a substrate}
\item This prevents \href{hyperglycemia.org}{hyperglycemia}
\end{itemize}
\end{itemize}
\item \textbf{Not subject to regulation by product inhibition}
\begin{itemize}
\item Regulated by glucokinase regulatory protein (GKRP) (\emph{outside scope of FoCS})
\end{itemize}
\end{itemize}
\subsection{\href{6\_phosphofructo\_1\_kinase.org}{6-Phosphofructo-1-kinase} (PFK-1)}
\label{sec:orgf576abb}
\begin{itemize}
\item Third enzyme in glycolysis
\item \textbf{Catalyzes the rate-limiting, committed step of glycolysis}
\item The \textbf{second} irreversible glycolytic reaction
\item \textbf{Allosterically regulated} by \textbf{energy charge} of the cell
\begin{itemize}
\item ↑[ATP] \textbf{inhibits} PFK-1
\item ↑[AMP] \textbf{stimulates} PFK-1
\begin{itemize}
\item This is because adenylate cyclase is a cytoplasmic enzyme and catalyzed in the reaction \(2 ADP -> ATP + AMP\)
\begin{itemize}
\item AMP:ATP ratio is the more sensitive indicator of energy charge
\end{itemize}
\end{itemize}
\end{itemize}
\item \textbf{Elevated levels of citrate} also allosterically \textbf{inhibits} PFK-1
\begin{itemize}
\item This is due to high [ATP] + [acetyl-CoA] in the mitochondria
\item Slows \href{glycolysis.org}{glycolysis} and allows for the synthesis of either \href{glycogen.org}{glycogen} or fatty acid synthesis
\end{itemize}
\end{itemize}
\subsection{\href{pyruvate\_kinase.org}{Pyruvate kinase}}
\label{sec:org202b41f}
\begin{itemize}
\item Final enzyme in the glycolytic pathway
\item Catalyzes the third irreversible + second substrate-level phosphorylation reaction
\item Produces \href{pyruvate.org}{pyruvate} from \href{phosphoenolpyruvate.org}{phosphoenolpyruvate} (PEP)
\item \textbf{Allosterically inhibited} by \href{atp.org}{ATP} in all tissues
\item When glucose is plentiful and PFK-1 is active, fructose 1,6-bisphosphate will feed forward and activate the enzyme
\item Deficiencies in the red blood cell isozyme causes hemolytic anemia
\end{itemize}
\subsubsection{The liver isozyme is uniquely regulated}
\label{sec:org8ddbeaf}
\begin{itemize}
\item Allosterically inhibited by ATP, \emph{alanine}, and \emph{long-chain fatty acids}
\begin{itemize}
\item All of which are plentiful during gluconeogenesis
\end{itemize}
\item A substrate of protein kinase A
\begin{itemize}
\item \textbf{Downregulated} by phosphorylation
\end{itemize}
\end{itemize}
\subsection{\href{pfk\_2.org}{6-Phosphofructo-2-kinase} (PFK-2)}
\label{sec:orgb97fea0}
\begin{itemize}
\item Liver enzyme that expresses \textbf{both} a \emph{kinase} \& \emph{phosphatase} activity
\item The product, \href{fructose\_2\_6\_bisphosphate.org}{fructose-2,6-bisphosphate}, is formed when the kinase catalytic site is active
\begin{itemize}
\item This product \textbf{uniquely regulates glycolysis vs. gluconeogenesis}
\begin{itemize}
\item Stimulates PFK-1 -> stimulates glycolysis
\item Inhibits fructose-1,6-bisphosphatase -> inhibits gluconeogenesis
\end{itemize}
\end{itemize}
\item The \emph{kinase} component is a \href{protein\_kinase\_a.org}{PKA} substrate
\begin{itemize}
\item \textbf{Inactivated by phosphorylation} -> \textbf{increases activity} of the \emph{phosphatase} component
\end{itemize}
\item In the fasting state (gluconeogenesis favored in liver) -> [\href{fructose\_2\_6\_bisphosphate.org}{F-2,6-bP}] is limited -> \textbf{PFK-1 not stimulated} -> further favoring of gluconeogenesis
\end{itemize}

\section{\href{pyruvate.org}{Pyruvate} metabolism}
\label{sec:org9a1e7a4}
\subsection{\href{pyruvate\_dehydrogenase\_complex.org}{Pyruvate dehydrogenase complex}}
\label{sec:org50ddae8}
\begin{itemize}
\item The mitochondrial PDH complex is allosterically regulated by product inhibition by acetyl-CoA \& NADH against their respective enzymes, as well as by covalent modification
\end{itemize}
\subsubsection{\href{pyruvate\_dh\_kinase.org}{Pyruvate DH kinase}}
\label{sec:org450e418}
\begin{itemize}
\item Phosphorylates Ser residues on the first enzyme of the PDH complex (E1)
\begin{itemize}
\item Results in significant \textbf{down-regulation of its activity}
\end{itemize}
\item Allosterically stimulated by the products of the reaction
\item Allosterically inhibited by its substrates
\item Also regulated by the energy charge <- kinase allosterically inhibited by ADP
\end{itemize}
\subsubsection{\href{pyruvate\_dh\_phosphatase.org}{Pyruvate DH phosphatase}}
\label{sec:org78be7bf}
\begin{itemize}
\item Activated by high concentrations of Ca\textsuperscript{2+} and Mg\textsuperscript{2​+} which \textbf{reverses the effects of PDH kinase} (i.e. dephosphorylates PDH)
\begin{itemize}
\item This \textbf{ensures glucose utilization}
\end{itemize}
\item Activation by Ca\textsuperscript{2+} primarily in skeletal muscle
\begin{itemize}
\item Contraction leads to Ca\textsuperscript{2+} release from cellular stores
\end{itemize}
\item Activating effect of Mg\textsuperscript{2+} due to \textbf{low mitochondrial concentrations of ATP}
\end{itemize}
\subsection{\href{alanine\_aminotransferase.org}{Alanine aminotransferase}}
\label{sec:orgbe8d546}
\begin{itemize}
\item A cytosolic enzyme
\item Catalyzes the readily reversible transfer of an amino group from alanine to α-ketoglutarate -> form pyruvate and glutamate
\begin{itemize}
\item Allows amino acids to be used as gluconeogenic precursors
\end{itemize}
\end{itemize}
\subsection{NAD\textsuperscript{+}-linked \href{lactate\_dehydrogenase.org}{lactate dehydrogenase}}
\label{sec:orgd8663eb}
\begin{itemize}
\item A cytosolic enzyme
\item Catalyzes a \textbf{freely reversible} redox reaction between \emph{lactate} and \emph{pyruvate}
\item \textbf{The only way in which lactate is utilized physiologically}
\begin{itemize}
\item Lactate released from anaerobic glyolysis occuring in RBCs + exercising muscle -> taken up by liver + used as a gluconeogenic substrate
\end{itemize}
\end{itemize}
\subsection{\href{pyruvate\_carboxylase.org}{Pyruvate carboxylase}}
\label{sec:orgde0934e}
\begin{itemize}
\item \href{biotin.org}{Biotin}-requiring mitochondrial enzyme
\item Catalyzes ATP-dependent carboxylation of pyruvate to oxaloacetate
\begin{itemize}
\item Then converted to PEP by PEP carboxylase for use in gluconeogenesis
\end{itemize}
\item Allosterically activated by acetyl-CoA
\begin{itemize}
\item The ability of Acetyl-CoA to inhibit PDH complex and activate pyruvate carboxylase ensures that \textbf{the pyruvate generated in the liver mitochondria will be used as a gluconeogenic substrate}
\end{itemize}
\end{itemize}
\section{\href{citric\_acid\_cycle.org}{Citric Acid Cycle}}
\label{sec:org882acce}
\subsection{\href{citrate\_synthase.org}{Citrate synthase}}
\label{sec:org92d8cf4}
\begin{itemize}
\item \textbf{Not allosterically regulated}
\begin{itemize}
\item \href{citrate.org}{Citrate} itself inhibits the enzyme by competing with \href{oxaloacetate.org}{oxaloacetate}
\end{itemize}
\item \href{succinyl\_coa.org}{Succinyl-CoA} is a strong inhibitor of citrate production
\begin{itemize}
\item Strictly competitive with \href{acetyl\_coa.org}{acetyl-CoA}
\item Noncompetitive with \href{oxaloacetate.org}{oxaloacetate}
\end{itemize}
\end{itemize}

\subsection{NAD\textsuperscript{+}-linked \href{isocitrate\_dehydrogenase.org}{isocitrate dehydrogenase}}
\label{sec:org16a4a1d}
\begin{itemize}
\item Catalyzes the \textbf{rate limiting and committed step of the TCA cycle}
\item Allosterically regulated by the \emph{energy charge} of the cell
\begin{itemize}
\item ATP inhibits
\item ADP stimulates
\end{itemize}
\item \href{adenosine\_diphosphate.org}{ADP} is the master energy sensor in the mitochondria
\begin{itemize}
\item Due to the absence of the appropriate adenylate cyclase isozyme
\item \textbf{↑[ADP] signals an energy deficit}
\begin{itemize}
\item Because ADP is the substrate of ATP synthesis via ATP synthase and oxidative phosphorylation
\end{itemize}
\item \href{adenosine\_monophosphate.org}{AMP} is master switch in the \href{cytoplasm.org}{cytoplasm}
\end{itemize}
\end{itemize}
\subsection{NAD\textsuperscript{+}-linked \href{α\_ketoglutarate\_dehydrogenase.org}{α-ketoglutarate dehydrogenase}}
\label{sec:org50a3325}
\begin{itemize}
\item Allostericaly regulated by \textbf{product inhibition}
\begin{itemize}
\item i.e. succinyl-CoA and NADH
\end{itemize}
\item Mechanism is \textbf{analogous to that of the \href{pyruvate\_dehydrogenase\_complex.org}{pyruvate dehydrogenase complex}}
\begin{itemize}
\item Consists of 3 enzymes
\begin{itemize}
\item \emph{E1} accepts a 5C acid rather than a 3C acid
\item \emph{E2} and \emph{E3} are identical to those found in the pyruvate DH complex
\item α-ketoglutarate DH is NOT subject to covalent modification by a tightly-associated kinase
\end{itemize}
\end{itemize}
\end{itemize}
\subsection{NAD\textsuperscript{+}-linked \href{malate\_dehydrogenase.org}{malate dehydrogenase}}
\label{sec:org24143df}
\begin{itemize}
\item Required for the \textbf{reversible oxidation of malate to oxaloacetate}
\begin{itemize}
\item Completes the TCA cycle
\item \textbf{Critical reaction in gluconeogenesis}
\begin{itemize}
\item Malate - α-ketoglutarate transporter moves oxaloacetate from the mitosol to the cytosol as a \textbf{gluconeogenic precursor}
\end{itemize}
\end{itemize}
\end{itemize}
\subsection{FAD-linked \href{succinate\_dehydrogenase.org}{Succinate dehydrogenase}}
\label{sec:orgd91ea7f}
\begin{itemize}
\item The \textbf{only FAD-linked dehydrogenase} in the TCA cycle
\item Embedded in inner mitochondrial membrane -> also called \emph{Complex II of ETC}
\begin{itemize}
\item Electrons generated by oxidation of succinate -> fumarate passed to \href{coenzyme\_q.org}{Coenzyme Q}
\begin{itemize}
\item Therefore, Succinate DH \textbf{is regulated by the availability of FAD}
\end{itemize}
\end{itemize}
\end{itemize}
\section{Fatty acid metabolism}
\label{sec:orgc5bc61b}
\subsection{\href{fatty\_acyl\_coa\_synthetase.org}{Fatty acyl-CoA synthetase}}
\label{sec:org3fa30e3}
\begin{itemize}
\item Required to ``activate'' cytoplasmic fatty acids for entry into mitochondria for \href{β\_oxidation.org}{β-oxidation}
\item Catalyzes formation of a fatty acyl-CoA
\end{itemize}
\subsection{\href{acetyl\_coa\_carboxylase.org}{Acetyl-CoA carboxylase}}
\label{sec:org5d5d628}
\begin{itemize}
\item \textbf{Anabolic} cytosolic enzyme
\item Catalyzes \textbf{rate-limiting and committed step of fatty acid synthesis}
\begin{itemize}
\item ATP-dependent carboxylation of acetyl-CoA to malonyl-CoA
\item \href{malonyl\_coa.org}{Malonyl-CoA} binds to and inhibits \href{carnitine\_palmitoyl\_acyltransferase\_1.org}{carnitine palmitoyl acyltransferase 1} (CPT1) -> prevents synthesized fatty acids from entering mitochondria and being oxidized
\end{itemize}
\item Requires \href{biotin.org}{biotin}
\item \href{protein\_kinase\_a.org}{PKA} substrate and \textbf{downregulated by phosphorylation}
\begin{itemize}
\item Dephosphorylation by \emph{insulin-induced phosphoprotein phosphatases} renews its activity
\end{itemize}
\end{itemize}
\subsection{\href{lipoprotein\_lipase.org}{Lipoprotein lipase}}
\label{sec:org0e1ca37}
\begin{itemize}
\item Expressed on \emph{capillary endothelium} primarily of muscle, adipose tissue and lactating mammary glands
\item \textbf{Catalyzes lipolysis of triglycerides circulating in \href{chylomicron.org}{chylomicrons}}
\begin{itemize}
\item \emph{Chylomicron} lipoprotein originating in gut that carries dietary fat and cholesterol to their appropriate destinations
\end{itemize}
\item Also targets \href{very\_low\_density\_lipoprotein.org}{very low-density lipoprotein} (VLDL)
\begin{itemize}
\item VLDL carries triglycerides and cholesterol from the liver to extrahepatic tissues
\end{itemize}
\item Both aforementioned lipoproteins carry \href{apocii.org}{apoCII} on their surface -> \textbf{facilitates interaction of lipoprotein lipase with lipoprotein}
\item \textbf{Induced by insulin in response to macronutrient digestion and formation of chylomicrons}
\end{itemize}
\subsection{\href{hormone\_sensitive\_lipase.org}{Hormone-sensitive lipase}}
\label{sec:org2a7e470}
\begin{itemize}
\item \textbf{Catabolic} enzyme
\item Undergoes reciptrocal regulation with intracellular \href{acetyl\_coa\_carboxylase.org}{acetyl-CoA carboxylase}
\begin{itemize}
\item \href{protein\_kinase\_a.org}{PKA} substrate
\begin{itemize}
\item Up-regulated by phosphorylation
\item Down-regulated by dephosphorylation
\end{itemize}
\end{itemize}
\item The \textbf{second} of three lipases \textbf{required for the complete hydrolysis of triglycerides} to 3 fatty acids and glycerol in adipose tissue
\begin{itemize}
\item \textbf{The isozyme restricted to adipose tissue is ONLY active in the fasting state}
\begin{itemize}
\item Consequently, glycerol and fatty acids produced via lipolysis are released from the adipocytes
\begin{itemize}
\item Glycerol taken up by liver -> used as \href{gluconeogenesis.org}{gluconeogenic} substrate
\item Released fatty acids -> taken-up by rapidly metabolizing tissues (e.g. liver) -> use fatty acids as energy source to drive \href{gluconeogenesis.org}{gluconeogenesis}
\end{itemize}
\end{itemize}
\end{itemize}
\end{itemize}
\subsection{\href{carnitine\_palmitoyl\_acyltransferase\_1.org}{CPT1} and \href{carnitine\_palmitoyl\_acyltransferase\_2.org}{CPT2}}
\label{sec:org36b0589}
\begin{itemize}
\item \textbf{Shuttle fatty acids (>12C) into mitochondrial matrix}
\item Use of \href{carnitine.org}{carnitine} in this shuttle ensures that the mitochondrial and cytosolic coenzyme A pools remain intact
\item \textbf{CPT 1 is blocked by malonyl-CoA} -> prevents fatty acids that are being synthesized in cytoplasm from entering the mitochondria for oxidation
\end{itemize}
\subsection{FAD\textsuperscript{+}-short-, medium- and long-chain \href{acyl\_coa\_dehydrogenase.org}{acyl-CoA dehydrogenases} (ACADs)}
\label{sec:org6acf7e6}
\begin{itemize}
\item \textbf{Catalyze the initial step in each cycle of fatty acid β-oxidation}
\begin{itemize}
\item Results in the introduction of a \emph{trans} double-bond between C2 (αC) and C3 (βC) of the acyl-CoA substrate
\end{itemize}
\item All types of ACADs are mechanistically similar and \textbf{require FAD as a coenzyme}
\end{itemize}

\section{\href{gluconeogenesis.org}{Gluconeogenesis}}
\label{sec:orgb667042}
\begin{itemize}
\item Anabolic process (i.e. making glucose) that occurs in the \textbf{fasting \href{liver.org}{liver}}
\item Most steps catalyzed by \textbf{reversible} glycolytic enzymes
\begin{itemize}
\item 4 enzymes required to replace the 3 irreversible steps of glycolysis
\end{itemize}
\end{itemize}
\subsection{\href{pyruvate\_carboxylase.org}{Pyruvate carboxylase}}
\label{sec:org38d0395}
\subsection{\href{pep\_carboxykinase.org}{PEP carboxykinase}}
\label{sec:orgfe2ccb3}
\begin{itemize}
\item Catalyzes a \textbf{GTP-dependent} reaction
\item \textbf{Converts \href{oxaloacetate.org}{OAA} into \href{phosphoenolpyruvate.org}{PEP}}
\item This + \href{pyruvate\_carboxylase.org}{Pyruvate carboxylase} required to convert pyruvate into PEP
\begin{itemize}
\item \textbf{Reverses action of pyruvate kinase}
\end{itemize}
\end{itemize}
\subsection{\href{fructose\_1\_6\_bisphosphatase.org}{Fructose 1,6-bisphosphatase}}
\label{sec:orgedef02f}
\begin{itemize}
\item Catalyzes \textbf{hydrolysis of F-1,6-bP to F6P}
\item Reverses \href{6\_phosphofructo\_1\_kinase.org}{PFK-1}
\item Allosterically inhibited by F-2,6-bP and AMP
\end{itemize}
\subsection{\href{glucose\_6\_phosphatase.org}{Glucose-6-phosphatase}}
\label{sec:org5817085}
\begin{itemize}
\item Catalyzes \textbf{hydrolysis of G6P to free glucose for release from liver}
\item Reverses \href{glucokinase.org}{glucokinase}
\end{itemize}
\subsection{NAD\textsuperscript{+}-linked lactate dehydrogenase}
\label{sec:org8773996}
\begin{itemize}
\item Converts lactate to pyruvate for entry into gluconeogenesis
\end{itemize}
\subsection{Alanine aminotransferase}
\label{sec:org7296429}
\begin{itemize}
\item Catalyzes conversion of alanine to pyruvate for entry into gluconeogenesis
\end{itemize}
\section{Additional important enzymes}
\label{sec:org6047707}
\subsection{\href{adenylate\_kinase.org}{Adenylate kinase}}
\label{sec:org5772b78}
\begin{itemize}
\item Cytoplasmic enzyme
\item Catalyzes \textbf{phosphoryl transfer between two ADP molecules to yield ATP and AMP}
\item \textbf{AMP:ATP ratio is the more sensitive indicator of energy charge}
\begin{itemize}
\item AMP signals that cell's energy stores are depleted
\end{itemize}
\end{itemize}
\subsection{Adenylate cyclase}
\label{sec:org60804f6}
\subsection{\href{protein\_kinase\_a.org}{Protein kinase A} (PKA)}
\label{sec:orge5bc357}
\begin{itemize}
\item Ser/Thr kinase
\item Effector enzyme resulting from glucagon and epinephrine induced signaling events
\begin{itemize}
\item Glucagon or epinephrine binds to respective membrane receptors -> \href{adenylate\_cyclase.org}{adenylate cyclase} catalyzed conversion of ATP to cAMP
\begin{itemize}
\item cAMP activates PKA by binding to its regulatory subunits -> release of catalytic subunits
\end{itemize}
\end{itemize}
\end{itemize}
\subsection{\href{camp\_phosphodiesterase.org}{cAMP phosphodiesterase}}
\label{sec:orgf23688f}
\begin{itemize}
\item \textbf{Hydrolyzes cAMP to 6'AMP}
\begin{itemize}
\item 5'AMP \textbf{does not signal} -> eliminates second messenger in glucagon signaling required to activate PKA
\begin{itemize}
\item \textbf{PKA activity down-regulated}
\end{itemize}
\end{itemize}
\end{itemize}
\subsection{\href{phosphoprotein\_phosphatase.org}{Phosphoprotein phosphatases}}
\label{sec:orgeaec5d8}
\begin{itemize}
\item Activated + induced by \href{insulin.org}{insulin}
\item \textbf{Reverses action (dephosphorylates) of PKA and other kinases}
\end{itemize}
\end{document}
