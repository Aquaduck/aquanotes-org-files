% Created 2021-08-31 Tue 11:52
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\author{Arif Ahsan}
\date{\textit{[2021-08-21 Sat]}}
\title{Information Transfer 1: Telomere Replication \& Maintenance}
\hypersetup{
 pdfauthor={Arif Ahsan},
 pdftitle={Information Transfer 1: Telomere Replication \& Maintenance},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.5)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section{DNA Replication in Eukaryotes}
\label{sec:org7951690}
\begin{itemize}
\item Human genome has 3 billion base pairs per haploid set of chromosomes
\begin{itemize}
\item 6 billion replicated during \href{s\_phase.org}{S phase} of the \href{cell\_cycle.org}{cell cycle}
\end{itemize}
\item \textbf{Multiple origins of replication on each eukaryotic chromosome}
\item Rate of replication \textasciitilde{} \textbf{100 nucleotides/s}
\begin{itemize}
\item Much \textbf{slower than prokaryotic replication}
\end{itemize}
\item \textbf{14 \href{dna\_polymerase.org}{DNA polymerases} in \href{eukaryote.org}{eukaryotes}}
\begin{itemize}
\item 5 are known to have major roles during replication and have been well studied:
\begin{itemize}
\item \href{dna\_polymerase\_ɑ.org}{pol α} (alpha)
\item pol β (beta)
\item pol γ (gamma)
\item \href{dna\_polymerase\_δ.org}{pol ẟ} (delta)
\item \href{dna\_polymerase\_ɛ.org}{pol ε} (epsilon)
\end{itemize}
\end{itemize}
\end{itemize}
\subsection{Steps in replication}
\label{sec:orgb8e0178}
The essential steps of replication are the same as in prokaryotes
\begin{enumerate}
\item Before replication begins, DNA has to be made available as a template
\begin{itemize}
\item \href{dna.org}{DNA} is bound to basic proteins known as \href{histone.org}{histones} to form stuctures called \href{nucleosome.org}{nucleosomes}
\item \href{histone.org}{Histones} must be removed and then replaced during the replication process -> accounts for lower replication rate in \href{eukaryote.org}{eukaryotes}
\item \href{chromatin.org}{Chromatin} undergoes chemical modifications so that DNA can slide off histones or be accessible to the enzymes of the DNA replication machinery
\item At the origin of replication, a pre-replication complex is made with other initiator proteins
\begin{itemize}
\item \href{helicase.org}{Helicase} and other proteins are then recruited to start replication
\end{itemize}
\end{itemize}
\item A \href{helicase.org}{helicase} opens up the DNA helix using energy from ATP hydrolysis
\begin{itemize}
\item Replication forks are formed at each replication origin as the DNA unwinds
\begin{itemize}
\item This causes overwinding/supercoiling
\begin{itemize}
\item Resolved by the action of \href{topoisomerase.org}{topoisomerases}
\end{itemize}
\end{itemize}
\end{itemize}
\item Primers are formed by the enzyme \href{primase.org}{primase}
\begin{itemize}
\item Using the primer, \href{dna\_polymerase.org}{DNA polymerase} can start synthesis
\end{itemize}
\item DNA pol α adds a short (20-30 nucleotide) DNA fragment to the RNA primer on both strands, and then hands off to a second polymerase (DNA Pol-δ or DNA Pol-ε)
\begin{itemize}
\item \href{dna\_polymerase\_δ.org}{DNA Pol-δ}  synthesizes lagging strand
\item \href{dna\_polymerase\_ɛ.org}{DNA Pol-ε}  synthesizes leading strand
\end{itemize}
\item A sliding clamp protein known as \href{proliferating\_cell\_nuclear\_antigen.org}{PCNA} (proliferating cell nuclear antigen) holds the DNA polymerase in place so that it does not slide off DNA
\item As pol δ runs into the primer RNA on the lagging strand, it displaces it from the DNA template
\begin{itemize}
\item Then removed by \href{rnase\_h.org}{RNase H} (aka flap endonuclease) and replaced with DNA nucleotides
\item Gaps between okazaki fragments sealed by \href{dna\_ligase.org}{DNA ligase}, which forms the phosphodiester bond
\end{itemize}
\end{enumerate}
\subsection{Differences between Prokaryotic and Eukaryotic replication}
\label{sec:orge9c32ab}
\begin{center}
\begin{tabular}{lll}
Property & Prokaryotes & Eukaryotes\\
\hline
Origin of replication & Single & Multiple\\
Rate of replication & 1000 nucleotides/s & 50-100 nucleotides/s\\
DNA polymerase types & 5 & 14\\
Telomerase & Not present & Present\\
RNA primer removal & DNA pol I & RNase H\\
Strand elongation & DNA pol III & Pol α, pol δ, pol ε\\
Sliding clamp & Sliding clamp & PCNA\\
\end{tabular}
\end{center}
\subsection{\href{telomere.org}{Telomere} replication}
\label{sec:orgecc1b96}
\begin{itemize}
\item Unlike prokaryotic chromosomes, eukaryotic chromosomes are linear
\begin{itemize}
\item When the replication fork reaches the end of the linear chromosome, there is \textbf{no way} to replace the primer on the 5' end of the lagging strand
\begin{itemize}
\item The DNA at the ends of the chromosome remain unpaired, and over time these get ends get progressively shorter as cells continue to divide
\end{itemize}
\end{itemize}
\item \emph{Telomeres} comprise repetitive sequences that code for no gene
\begin{itemize}
\item In humans, this is a six-base-pair sequence TTAGGG which is repeated 100 to 1000 times in the telomere regions
\item This in effect \textbf{protects the genes from getting deleted as cells continue to divide}
\end{itemize}
\item \href{telomerase.org}{Telomerase} is responsible for adding telomeres
\begin{itemize}
\item Contains two parts: a \textbf{catalytic part} and a \textbf{built-in RNA template}
\begin{itemize}
\item Attaches to end of a chromosome via DNA nucleotides complementary to the RNA template
\begin{itemize}
\item Corresponding nucleotides are added on the 3' end of the DNA strand
\item Once sufficiently elongated, DNA pol adds the complementary nucleotides to the other strand -> chromosomes successfully replicated
\end{itemize}
\end{itemize}
\item Telomerase is typically active in germ cells and adult stem cells, and \textbf{not} active in adult somatic cells
\end{itemize}
\end{itemize}
\subsection{\href{telomerase.org}{Telomerase} and aging}
\label{sec:org59b5755}
\begin{itemize}
\item Cells that undergo cell division continue to have their telomeres shortened because most somatic cels do not make telomerase
\begin{itemize}
\item Means that \textbf{telomere shortening is associated with aging}
\end{itemize}
\item In 2010, scientists found that telomerase cna reverse some age-related conditions in mice
\begin{itemize}
\item Telomerase-deficient mice had muscle atrophy, stem cell depletion, organ system failure, and impaired tissue injury responses
\item Following reactivation of telomerase -> caused extension of telomeres, reduced DNA damage, reversed neurodegeneration, and improved function of testes, spleen, and intestines
\end{itemize}
\item \href{cancer.org}{Cancer} is characterized by uncontrolled cell division of abnormal cells
\begin{itemize}
\item Cells accumulate mutations, proliferate uncontrollably, and can migrate to different parts of the body through a process called \href{metastasis.org}{metastatis}
\item Scientists have observed that \textbf{cancerous cells have considerably shortened telomeres and that telomerase is active in these cells}
\begin{itemize}
\item Only \textbf{after telomeres were shortened} did telomerase become active
\begin{itemize}
\item If action of telomerase in these cells can be inhibited -> cancerous cells potentially stopped from further division
\end{itemize}
\end{itemize}
\end{itemize}
\end{itemize}
\end{document}
