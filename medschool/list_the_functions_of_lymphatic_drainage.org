#+title: List the functions of lymphatic drainage.
#+date: [2021-10-09 Sat]
#+hugo_tags: medschool learning_objective

* From [[file:lymphatic_and_immune_systems_ppt.org][Lymphatic and Immune Systems PPT]]
** Functions of [[file:lymph.org][lymphatic]] flow
- Slow movement of lymph -> *filter out any foreign or undesirable material that may have inadvertently reached the interstitial fluid*
  + This filtering action is performed by [[file:lymph_node.org][lymph nodes]] - the *major sites of activation of the adaptive immune system*
- The arrangement of lymph and [[file:lymph_node.org][lymph nodes]] is designed to *activate the immune system before antigens and invading organisms reach the blood*
- An additional function unrelated to immunity is the *transport of [[file:lipoprotein.org][lipoproteins]] absored in the G.I. tract*, which *exclusively enter the blood via lymphatics*
