#+title: List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)
#+date: [2021-10-23 Sat]
#+hugo_tags: medschool learning_objective

* [[file:triacylglycerol.org][Triacylglycerol]]
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- The *insolubility of triacylglycerols* poses a significant problem for both:
  + *The absorption and utilization of dietary lipids*
  + *The mobilization of triacylglycerls stored in adipocytes*
*** Pathway of triacylglycerol breakdown
- Absorption of dietary fats is dependent on the *presence of amphipathic bile salts* -> creation of small /micelles/ of hydrophobic fat
- Triacylglycerol in micelles is then cleaved by lipases into mono- and di-acylglycerol, free fatty acids and glycerol -> absorbed by /enterocytes/ (cells of the intestinal lining)
  + Digested triacylglycerol (except for short- and medium-chain fatty acids) are *resynthesized in the ER* along with phospholipids and cholesterol esters
    - Short- and medium-chain fatty acids *diffuse directly across the enterocyte into portal capillaries*
- Newly synthesized triacylglycerol + cholesteryl esters are very hydrophobic -> aggregate in aqueous cytoplasm
  + Must be *packaged* as components of lipid droplets ([[file:lipoprotein.org][lipoproteins]]) surrounded by a thin unilamellar, amphipathic layer composed of phospholipids, unesterified cholesterol and a molecule of apoB-48
    - Molecules of apoCII and apoCIII are also integrated
- The formed chylomicrons are then *exocytosed into the lymphatics* -> *enter venous circulation*
- Chylomicrons indicate a fed state -> [[file:insulin.org][insulin]] regulates uptake of [[file:dietary_lipid.org][fat]] into tissues
  + [[file:insulin.org][Insulin]] *increases [[[file:lipoprotein_lipase.org][lipoprotein lipase]]] -> activates fat uptake*
    - Done through its effects on gene transcription
  + [[file:insulin.org][Insulin]] also *increases glucose uptake in skeletal muscle and adipose tissue*
- LPL will cleave the triacylglycerol to *either* /three [[file:fatty_acid.org][fatty acids]] and glycerol/ or /two fatty acids and a [[file:monoacylglycerol.org][monoacylglycerol]]/
  + Fatty acids and monoacylglycerol will diffuse freely into the endothelial cell -> [[file:glycerol.org][glycerol]] taken up by the [[file:liver.org][liver]]
- [[file:fatty_acid.org][Fatty acids]] will bind to various /fatty acid binding proteins/ -> able to traverse the cell and enter the tissue beneath
  + In muscle, fatty acids will be oxidized for energy and/or stored
  + In lactating [[file:mammary_gland.org][mammary glands]], fatty acids + monoacylglycerol become components of [[file:milk.org][milk]]
*** Mobilization of triacylglycerol stored in [[file:adipose_cell.org][adipocytes]]
- *Triacylglycerol energy reserve is stored in adipocytes in the fed state*
  + *Decreased concentrations of [[file:insulin.org][insulin]] are essential for the successful initiation of this process*
    - Activation of both this process & major enzymes required for [[file:lipolysis.org][lipolysis]] is accomplished by [[file:protein_kinase_a.org][PKA]]
    - Therefore, *cAMP-mediated activation of PKA is required*
- /Perilipin/ (aka /lipid droplet-associated protein/) coats the surface of intracellular lipid droplets -> protects them from activate intracellular lipases
  + *PKA-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored fatty acids*
- Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> *catalyzes release of a [[file:fatty_acid.org][fatty acid]] + formation of [[file:2_3_diacylglycerol.org][2,3-DAG]]*
- Phosphorylation of HSL -> cleaves 2,3-DAG to a fatty acid + monoacylglycerol -> monoacylglycerol lipase releases fatty acid -> formation of glycerol
- *Ultimately, 3 fatty acids are mobilized/liberated and glycerol is formed*
  + Fatty acids exit adipose tissue via diffusion and bind reversibly to plasma [[file:albumin.org][albumin]]
  + Glycerol taken up by the liver
*** Mobilization of stored triacylglycerol in [[file:muscle.org][muscle]]
:PROPERTIES:
:ID:       005c9565-47a8-4b44-9e21-c676ff7a5ab2
:END:
- Skeletal muscle obtains dietary fatty acids from chylomicrons that can be "re-synthesized" into triacylglycerol due to the muscle's regular uptake of glucose
  + These fat stores can be mobilized in the muscle cell via skeletal muscle contractions or in a stressed situation by [[file:epinephrine.org][Epinephrine]]
#+attr_html: :width 500
#+caption: Fatty acid mobilization mediated via skeletal muscle contraction vs. epinephrine
[[attachment:_20211024_191932screenshot.png]]
*** [[file:lipolysis.org][Fatty acid catabolism]]
:PROPERTIES:
:ID:       dd3d741b-4b1a-44a8-87d8-1af67549fc85
:END:
- Can be divided into *3 stages*:
  1) /Investment/ to prepare them for ->
  2) /Transport/ into the mitochondrial matrix wherein they will undergo ->
  3) /Oxidation/
#+attr_html: :width 700
[[attachment:_20211024_192624screenshot.png]]
**** Investment phase
- Fatty acids are first activated by /acyl-CoA synthetases/ in the outer mitochondrial membrane
  + Catalyze the formation of a thioester linkage between the fatty acid and CoA-SH to form a fatty acyl-CoA
  + This is coupled to the cleavage of ATP -> AMP + PP_{i} -> *two ATP molecules required to drive this reaction*
**** Transport phase
- Passage of fatty acyl-CoA across the inner mitochondrial membrane requires /carnitine/
- Fatty aycl-carnitine ester is formed by the action of /carnitine acyltransferase I/ once inside the inner membrane space
  + Becomes a substrate for /carnitine acyltransferase II/ -> catalyzes transfer of acyl to CoA-SH in matrix -> release carnitine for a second round of fatty acid transport
- *Rate-limiting and commits the fatty acyl-CoA to oxidation in the matrix*
#+attr_html: :width 700
[[attachment:_20211024_193058screenshot.png]]
**** Oxidation phase
- Occurs in three stages:
  1) Beta-oxidation
  2) Oxidation of acetyl-CoA in TCA cycle -> 3 NADH, 1 FADH_{2}, 1 GTP
  3) ETC -> 9 ATP
     - Electrons from 3 NADH and 1 FADH_{2}
- *A problem with either the [[file:biotin.org][biotin]]-dependent enzyme ([[file:propionyl_coa_carboxylase.org][propionyl-CoA carboxylase]]) or the [[file:cobalamin.org][B12]]-dependent enzyme ([[file:methylmalonyl_coa_mutase.org][methylmalonyl-CoA mutase]]) in beta-oxidation -> profound [[file:metabolic_acidosis.org][metabolic acidosis]] caused by:*
  + Increased [propionic acid] = /biotin deficiency/
  + Increased [methylmalonic acid] = /vitamin B12 deficiency/
  + Leads to *mental deficits*
* Gastrointestinal [[file:lipase.org][lipase]]
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- Triacylglycerol in micelles is then cleaved by lipases into mono- and di-acylglycerol, free fatty acids and glycerol -> absorbed by /enterocytes/ (cells of the intestinal lining)
* [[file:chylomicron.org][Chylomicrons]]
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- The *carriers of [[file:dietary_lipid.org][dietary lipids]] in the [[file:blood.org][blood]]*
- Molecules of [[file:apolipoprotein_cii.org][apo-CII]] and [[file:apolipoprotein_ciii.org][apo-CIII]] are also integrated into the chylomicron "membrane"
- Formed chylomicrons are *exocytosed into the lymphatics* -> *enter venous circulation*
- Chylomicrons *indicate a fed state*
- [[file:skeletal_muscle.org][Skeletal muscle]] obtains dietary fatty acids from chylomicrons that can be "re-synthesized" into triacylglycerol due to the muscle's regular uptake of glucose
* Apolipoproteins
* apoB48 & apoCII
** [[file:apolipoprotein_b_48.org][ApoB48]]
*** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- Newly synthesized triacylglycerol + cholesteryl esters are very hydrophobic -> aggregate in aqueous cytoplasm
  + Must be *packaged* as components of lipid droplets ([[file:lipoprotein.org][lipoproteins]]) surrounded by a thin unilamellar, amphipathic layer composed of phospholipids, unesterified cholesterol and a molecule of apoB48
- *The structural protein of [[file:chylomicron.org][chylomicrons]]*
*** From [[file:amboss.org][Amboss]]
- Mediates the secretion of [[file:chylomicron.org][Chylomicron]] particles that originate from the intestine into the lymphatics
- A component of [[file:chylomicron.org][chylomicrons]]
** [[file:apocii.org][ApoCII]]
*** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- ApoCII plays a *crucial role in delivering dietary fat to extrahepatic tissues*
- ApoCII anchors the chylomicron to the LPL molecules expressed by capillary endothelial cells
*** From [[file:amboss.org][Amboss]]
- Cofactor for [[file:lipoprotein_lipase.org][Lipoprotein lipase]]
- A component of [[file:chylomicron.org][chylomicrons]], [[file:very_low_density_lipoprotein.org][VLDL]], and [[file:high_density_lipoprotein.org][HDL]]
* [[file:lipoprotein_lipase.org][Lipoprotein lipase]]
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- [[file:insulin.org][Insulin]] *increases [[[file:lipoprotein_lipase.org][lipoprotein lipase]]] -> activates fat uptake*
  + Done through its effects on gene transcription
- LPL will cleave the triacylglycerol to *either* /three fatty acids and glycerol/ or /two fatty acids and a monoacylglycerol/
- The LPL isozyme of adipose tissue has the *largest K_{m} -> [[file:adipose_cell.org][adipose tissue's]] uptake of [[file:dietary_lipid.org][dietary fat]] is a last resort*
  + *Adipose tissue stores fat* -> least likely to utilize it
  + *LPL is not expressed by the [[file:liver.org][liver]] or the [[file:brain.org][brain]]* -> NO dietary fat reaches either of those tissues
- Tethering the LPL to the cell via a long polysaccharide chain consisting of /heparan sulfates/ -> *significantly improves the likelihood of an [[file:apocii.org][apoCII]]:LPL interaction within the capillary*
* [[file:perilipin.org][Perilipin]]
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- /Perilipin/ (aka /lipid droplet-associated protein/) coats the surface of intracellular lipid droplets -> protects them from activate intracellular lipases
  + *PKA-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored fatty acids*
- Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> *catalyzes release of a [[file:fatty_acid.org][fatty acid]] + formation of 2,3-DAG*
* [[file:hormone_sensitive_lipase.org][Hormone-sensitive lipase]]
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- *PKA-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored fatty acids*
- Phosphorylation of HSL -> cleaves 2,3-DAG to a fatty acid + monoacylglycerol -> monoacylglycerol lipase releases fatty acid -> formation of glycerol
* [[file:fatty_acyl_coa.org][Fatty acyl-CoA]]
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- In the *investment phase of fatty acid oxidation*
- Fatty acids are first activated by /acyl-CoA synthetases/ in the outer mitochondrial membrane
  + Catalyze the formation of a thioester linkage between the fatty acid and CoA-SH to form a fatty acyl-CoA
  + This is coupled to the cleavage of ATP -> AMP + PP_{i} -> *two ATP molecules required to drive this reaction*
* [[file:fatty_acyl_coa_synthetase.org][Fatty acyl-CoA synthetase]]
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- In the *investment phase of fatty acid oxidation*
- Fatty acids are first activated by /acyl-CoA synthetases/ in the outer mitochondrial membrane
  + Catalyze the formation of a thioester linkage between the fatty acid and CoA-SH to form a fatty acyl-CoA
  + This is coupled to the cleavage of ATP -> AMP + PP_{i} -> *two ATP molecules required to drive this reaction*
* [[file:carnitine.org][Carnitine]]
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- Passage of fatty acyl-CoA across the inner mitochondrial membrane requires /carnitine/
* [[file:albumin.org][Albumin]]-bound fatty acids
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- *Two major purposes:*
  1) Half-life of albumin-bound fatty acid is *~3 min -> taken up and oxidized by rapidly metabolizing tissues due to fasting state*
     - E.g. [[file:muscle.org][muscle]] and [[file:kidney.org][kidney]]
  2) Fasting state -> *fatty acids enter the [[file:liver.org][liver]] as the energy source needed to drive [[file:gluconeogenesis.org][gluconeogenesis]]*
     - *Released [[file:glycerol.org][glycerol]] is used primarly by the [[file:liver.org][liver]] as a [[file:gluconeogenesis.org][gluconeogenic]] substrate*
* [[file:β_oxidation.org][Beta-oxidation]]
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- First stage of oxidation of [[file:fatty_acid.org][fatty acids]]
- Oxidation of the fatty acid at the beta-carbon, followed by a hydration, an oxidation, and a thiolysis
- Removes carbons 1 and 2 as a molecule of acetyl-CoA
- One round of beta-oxidation -> 1 molecule of NADH and FADH_{2}
- Odd-chain fatty acids produce acetyl-CoA *and* propionyl-CoA
- *A problem with either the [[file:biotin.org][biotin]]-dependent enzyme ([[file:propionyl_coa_carboxylase.org][propionyl-CoA carboxylase]]) or the [[file:cobalamin.org][B12]]-dependent enzyme ([[file:methylmalonyl_coa_mutase.org][methylmalonyl-CoA mutase]]) in beta-oxidation -> profound [[file:metabolic_acidosis.org][metabolic acidosis]] caused by:*
  + Increased [propionic acid] = /biotin deficiency/
  + Increased [methylmalonic acid] = /vitamin B12 deficiency/
  + Leads to *mental deficits*
* [[file:carnitine.org][Carnitine]]
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- Passage of fatty acyl-CoA across the inner mitochondrial membrane requires /carnitine/
* Carnitine acyltransferase I (CAT-1) and CAT-II
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
*** [[file:carnitine_palmitoyl_acyltransferase_1.org][CAT-1]]
- [[file:fatty_acyl_carnitine_ester.org][Fatty acyl-carnitine ester]] is formed by the action of /carnitine acyltransferase I/ once inside the inner membrane space
*** [[file:carnitine_palmitoyl_acyltransferase_2.org][CAT-2]]
- [[file:fatty_acyl_carnitine_ester.org][Fatty acyl-carnitine ester]] becomes a substrate for /carnitine acyltransferase II/ -> catalyzes transfer of acyl to CoA-SH in matrix -> release [[file:carnitine.org][carnitine]] for a second round of fatty acid transport
* FAD- and NAD^{+}-linked dehydrogenases
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- Recognize very long-, long-, middle-, or small-chain fatty acids as substrates -> carry out oxidation reactions in [[file:β_oxidation.org][beta-oxidation]]
* [[file:propionyl_coa.org][Propionyl-CoA]]
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- Produced via the beta-oxidation of odd-chain fatty acids
- Undergoes an ATP- and biotin-dependent carboxylation reaction to form [[file:d_methylmalonyl_coa.org][D-methylmalonyl-CoA]] -> converted to L-methylmalonyl-CoA
* [[file:propionyl_coa_carboxylase.org][Propionyl-CoA carboxylase]]
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- Catalyzes the formation of D-methylmalonyl-CoA from [[file:propionyl_coa.org][Propionyl-CoA]]
* [[file:l_methylmalonyl_coa.org][L-methylmalonyl-CoA]]
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- Propionyl-CoA undergoes an ATP- and biotin-dependent carboxylation reaction to form D-methylmalonyl-CoA -> converted to L-methylmalonyl-CoA
- L-methylmalonyl-CoA converted to [[file:succinyl_coa.org][succinyl-CoA]] via [[file:cobalamin.org][B12]] dependent isomerization catalyzed by [[file:methylmalonyl_coa_mutase.org][Methylmalonyl-CoA mutase]]
  + Succinyl-CoA then oxidized in TCA cycle
* [[file:methylmalonic_acid.org][Methylmalonic acid]]
** From [[file:fat_metabolism_in_muscle_adipose_tissue.org][Fat Metabolism in Muscle & Adipose Tissue]]
- Increased [methylmalonic acid] is indicative of a [[file:cobalamin.org][vitamin B12]] deficiency

