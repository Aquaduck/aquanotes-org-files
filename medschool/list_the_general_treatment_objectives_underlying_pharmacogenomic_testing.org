#+title: List the general treatment objectives underlying pharmacogenomic testing
#+date: [2021-09-25 Sat]
#+hugo_tags: medschool learning_objective

* From [[file:pharmacogenetics_pre_learning_material.org][Pharmacogenetics Pre-learning Material]]
** Precision (targeted) therapies
- Medicines *designed to exploit specific pathogenetic genetic variations* in order to have a therapeutic effect
