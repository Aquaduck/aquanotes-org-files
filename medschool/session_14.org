#+title: Session 14
#+date: [2021-07-29 Thu]
#+hugo_tags: medschool jumpstart
* Worksheet
1) Top
   - Open Reading Frame is on the SENSE strand -> corresponds to mRNA strand
2) Top
3) Bottom
4) AUG CGU AGU CUU GGU CCA GAG UAA
5) (N-terminus)-Met-Arg-Ser-Leu-Gly-Pro-Glu-(C-terminus)
6) Elongation -> protein strand is generated but shortened
7) Question parts:
   a) 5' AUG CCC CUU AAA GAG UUU ACA UAU UGC UGG CGU UAA 3'
   b) (N-Terminus)-Met-Pro-Leu-Lys-Glu-Phe-Thr-Tyr-Cys-Arg-Arg-(C-terminus)
8) /Same method as 7/
   a) /mRNA sequence/
   b) /AA sequence/
   c) Point mutation
   d) Nonconservative
      - Leu is nonpolar, Arg is basic
* Application Questions
1) D
2) C
3) D
4) B
   - Acidic AA -> nonpolar AA
   - nonpolar interior, polar exterior
   - Would ruin structure of protein
5) C
