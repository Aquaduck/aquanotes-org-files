#+title: Describe the vessels supplying the conduction system of the heart and the clinical presentation following damage to these.
#+date: [2021-10-28 Thu]
#+hugo_tags: medschool learning_objective

* From [[file:clinically_oriented_anatomy.org][Clinically Oriented Anatomy]]
** Coronary occlusion and conducting system of [[file:heart.org][Heart]]
- Damage to conducting system of heart -> disturbances of [[file:cardiac_muscle.org][cardiac muscle]] contraction
- LAD gives rise to septal branches supplying AV bundle in most people
- Branches of RCA supply both SA and AV nodes (60% of the time, 40% of people have supply from LCA instead) -> conducting system affected by occlusion -> heart block
  + Ventricles will begin to contract independently at their own rate of 25-30 times per minute (much slower than normal)
  + Atria continue to contract at normal rate if SA node is spared, but impulse from SA does not reach ventricles
- Damage to one bundle branch -> bundle-branch block
  + Excitation passes along unaffected branch and causes a normally timed systole *of that ventricle only*
  + Impulse then spreads to other ventricle via myogenic (muscle propagated) conduction -> *late asynchronous contraction*
  + In this case, a cardiac pacemaker may be implanted to increase the ventricular rate of contraction to 70-80 per minute

