#+title: The role of TRADD in death receptor signaling
#+date: [2021-09-27 Mon]
#+hugo_tags: medschool source

* [[file:tnfr1_associated_death_domain_protein.org][TRADD]] in [[file:tumor_necrosis_factor.org][TNF]]/[[file:tnfr1.org][TNFR1]] signalling
- The default signaling pathway activating by the binding of TNF alpha is the induction of [[file:nfκb.org][NFκB]] -> responsible for *cell survival and growth*
- TNFR1 induces cell death (apoptosis AND necrosis) *only when [[file:nfκb.org][NFκB]] activation is impaired*
- Dependence of TNFR1 signaling on TRADD appears to be *cell-type specific*

* [[file:tnfr1_associated_death_domain_protein.org][TRADD]] in [[file:apo2_ligand.org][TRAIL]]/TRAILR Signaling
- Both TRADD and [[file:receptor_interacting_protein.org][RIP]] were demonstrated to be recruited to the TRAILR complex in the [[file:death_inducing_signaling_complex.org][DISC]]
  + The recruitment of [[file:fas_associated_death_domain.org][FADD]] was increased in the absence of TRADD, indicating that:
    1) Recruitment of RIP to the TRAILR complex is TRADD-dependent
    2) *The binding of TRADD and FADD to TRAILR is most likely independent and competitive*

* [[file:tnfr1_associated_death_domain_protein.org][TRADD]] and Other Death Receptors
- [[file:fas.org][Fas]]'s main function is to induce apoptosis via formation of a DISC following binding of [[file:fas_ligand.org][FasL]]
  + Includes FADD and caspase-8
- TRADD was never reported as a component of the FAS DISC
  + In line with this, apoptosis induced by anti-FAS antibody was unaffected in TRADD-deficient mouse embryonic fibroblasts and thymocytes

