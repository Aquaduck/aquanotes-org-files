#+title: Describe the common clinical procedures involving the heart. Include a description of the procedure(s) and the structure(s) that are accessed or repaired.
#+date: [2021-10-28 Thu]
#+hugo_tags: medschool learning_objective

* From [[file:clinically_oriented_anatomy.org][Clinically Oriented Anatomy]]
** Clinical procedures related to [[file:heart.org][heart]] structures:
*** [[file:cardiac_catheterization.org][Cardiac catheterization]]:
- Radiopaque catheter inserted into peripheral vein (e.g. femoral vein) and passed under fluoroscopic control into the [[file:right_atrium.org][right atrium]], [[file:right_ventricle.org][right ventricle]], [[file:pulmonary_trunk.org][pulmonary trunk]] and [[file:pulmonary_artery.org][pulmonary arteries]] respectively
- Intracardiac pressures can be recorded and blood samples may be removed
- If radopaque contrast medium is injected -> can be followed through heart and great vessels using serially exposed X-ray films
*** [[file:coronary_angiography.org][Coronary angiography]]
- Coronary arteries visualized with coronary arteriograms
- Long narrow catheter passed into ascending aorta via femoral artery
- Under fluoroscopic control, tip of catheter placed just inside opening of a coronary artery
- Small injection of radiopaque contrast material made -> cineradiographs taken to show lumen of artery and its branches, as well as any stenotic areas present
- Noninvasive CT or MR angiography is replacing invasive conventional methods
*** [[file:coronary_artery_bypass.org][Coronary artery bypass]]
- Segment of artery or vein is connected to ascending aorta or to proximal part of a coronary artery -> connected to coronary artery distal to stenosis
- Great saphenous vein is commonly harvested for coronary bypass because:
  1) Has a diameter equal to or greater than that of oronary arteries
  2) Can be easily dissected from lower limb
  3) Offers relatively lengthy portion with minimum occurrence of valves or branching
- Use of radial artery in bypass surgery has become more common
- Coronary bypass graft shunts blood from aorta to a steonic coronary artery -> increase flow distal to obstrution
  + i.e. detour around stenotic area
- Revascularization of myocardium may also be achieved by surgically anastomosing an internal thoracic artery with a coronary artery
