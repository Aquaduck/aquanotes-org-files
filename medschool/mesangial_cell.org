#+title: Mesangial cell
#+date: [2021-10-31 Sun]
#+hugo_tags: medschool concept

* Concept links
- Remove trapped material from basement membrane of the capillaries in the [[file:glomerulus.org][Glomerulus]]
