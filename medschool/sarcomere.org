#+title: Sarcomere
#+date: [2021-09-12 Sun]
#+hugo_tags: medschool concept

* From Amboss
- Smallest functional unit of striated muscle fiber
- One sarcomere is the area between two Z bands
- During contraction, sarcomeres shorten while myofilaments stay the same length
