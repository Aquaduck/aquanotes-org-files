#+title: Isocitrate dehydrogenase
#+date: [2021-08-21 Sat]
#+hugo_tags: medschool concept
#+roam_alias: "Isocitrate DH"
* Concept links
- Rate-limiting and commited step of the [[file:citric_acid_cycle.org][TCA cycle]]
- Linked to [[file:nadh.org][NAD^{+}]]
