#+title: Pericyte
#+date: [2021-09-14 Tue]
#+hugo_tags: medschool concept

* From [[file:brs_cell_biology_histology.org][BRS Cell Biology & Histology]]
- *Pericytes* are derived from embryonic mesenchymal cells and may retain a pluripotential role
  + Possess characteristics of endothelial cells as well as smooth muscle cells
    - Contain [[file:actin.org][actin]], [[file:myosin.org][myosin]], and [[file:tropomyosin.org][tropomyosin]] -> suggests they play a role in contraction
      + This + proximity to capillaries -> could function to *modify capillary blood flow*
  + Smaller than fibroblasts and *are located along most capillaries*
    - Lie within their own basal lamina
  + During blood vessel formation and repair, they may differentiate into smooth muscle cells or endothelial cells
  + In response to injury, may give rise to endothelial cells, fibroblasts, and smooth muscle cells of blood vessel walls
