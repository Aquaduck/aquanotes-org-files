#+title: Procaspase-9
#+date: [2021-09-27 Mon]
#+hugo_tags: medschool concept

* Concept links
- The zymogen precursor to [[file:caspase_9.org][Caspase-9]]
