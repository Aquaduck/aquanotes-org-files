#+title: Loop of Henle
#+date: [2021-10-31 Sun]
#+hugo_tags: medschool concept

* Concept links
- Component of the [[file:nephron.org][Nephron]] responsible for reabsorption of [[file:water.org][water]] and [[file:sodium.org][sodium]]
