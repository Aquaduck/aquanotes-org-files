#+title: Collecting duct system
#+date: [2021-11-01 Mon]
#+hugo_tags: medschool concept

* Concept links
- A component of the [[file:nephron.org][Nephron]] that follows the DCT and leads into the [?]

