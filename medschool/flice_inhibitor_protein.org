#+title: FLICE-inhibitor protein
#+date: [2021-09-26 Sun]
#+hugo_tags: medschool concept
#+roam_alias: FLIP

* Concept links
- Dimerizes with [[file:caspase_8.org][Caspase-8]] in the [[file:death_inducing_signaling_complex.org][DISC]]
- Restrains [[file:extrinsic_pathway_of_apoptosis.org][Extrinsic pathway of apoptosis]] by inhibiting [[file:fadd_like_interleukin_1b_converting_enzyme.org][FLICE]]
