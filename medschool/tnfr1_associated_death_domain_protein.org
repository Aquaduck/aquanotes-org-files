#+title: TNFR1-associated death domain protein
#+date: [2021-09-27 Mon]
#+hugo_tags: medschool concept
#+roam_alias: TRADD

* Concept links
- Contains a [[file:death_domain.org][Death domain]]
