#+title: Glucose 6-phosphate
#+date: [2021-08-19 Thu]
#+hugo_tags: medschool concept
#+roam_alias: G6P g6p

* Concept links
- Signals [[file:muscle.org][muscle]] cells to start [[file:glycogenesis.org][synthesizing]] [[file:glycogen.org][glycogen]]
