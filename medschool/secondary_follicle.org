#+title: Secondary follicle
#+date: [2021-10-12 Tue]
#+hugo_tags: medschool concept

* Concept links
- A kind of [[file:follicle_of_the_lymph_node.org][Follicle of the lymph node]] that *has a germinal center*
