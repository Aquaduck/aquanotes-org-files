#+title: Thrombocytosis
#+date: [2021-10-16 Sat]
#+hugo_tags: medschool concept patho

* Concept links
- A condition where there are too many [[file:thrombocyte.org][platelets]]

