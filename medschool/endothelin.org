#+title: Endothelin
#+date: [2021-10-18 Mon]
#+hugo_tags: medschool concept

* Concept links
- Stimulated by:
  + [[file:angiotensin_ii.org][Angiotensin II]]
  + [[file:bradykinin.org][Bradykinin]]
  + [[file:epinephrine.org][Epinephrine]]
