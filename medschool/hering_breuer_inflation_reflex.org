#+title: Hering-Breuer inflation reflex
#+date: [2021-10-26 Tue]
#+hugo_tags: medschool concept

* Concept links
- Inhibits [[file:inspiration.org][Inspiration]] to prevent overinflation of the [[file:lung.org][lungs]]
