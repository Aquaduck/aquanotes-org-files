#+title: Session 7
#+date: [2021-07-20 Tue]
#+hugo_tags: medschool jumpstart
* gRAT
1) A
2) D
3) B
4) E
5) D
6) B
7) B
8) D
9) C
   - Digenic inheritance: need bad gene in /two/ locations to get different phenotype
10) D

* Application Packet
1) D
2) C
3) B
4) D
5) Heterozygous -> 25% that both recessive genes passed and skips a generation
6) C
7) C
8) E
9) C
10) Expressivity -> continuum of how it affects people
