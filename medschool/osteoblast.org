#+title: Osteoblast
#+date: [2021-09-30 Thu]
#+hugo_tags: medschool concept

* Concept links
- Lay down new [[file:osteoid.org][Osteoid]] and calcify it to form new [[file:bone.org][Bone]]
