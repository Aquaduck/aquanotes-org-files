#+title: Describe the components of a credible guideline
#+date: [2021-11-05 Fri]
#+hugo_tags: medschool learning_objective

* From [[file:ebm_practice_guidelines_handout.org][EBM Practice Guidelines Handout]]
** Components of a credible [[file:clinical_practice_guideline.org][guideline]]
1) Utilizing a systematic literature review
2) Establishing transparency and disclosing the methods used for all development steps
3) A multidisciplinary development group
4) Disclosure and management of both financial and non-financial conflicts of interests
5) Clear and unambiguous guideline recommendations
6) Using a specific grading systems to rate the strength evidence and recommendations
7) External peer review
8) Updating guidelines
