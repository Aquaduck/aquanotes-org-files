#+title: Discuss the mechanisms by which phosphorylation is involved in turning off the cAMP signal pathway.
#+date: [2021-09-04 Sat]
#+hugo_tags: medschool learning_objective
[[file:describe_the_several_mechanisms_involved_in_turning_off_the_camp_second_messenger_system_and_why_the_off_signal_is_important.org::*Inactivate the receptor][Inactivate the receptor]]
