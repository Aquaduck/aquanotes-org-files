#+title: Explain the determinants of blood flow in the peripheral circulation (pressure difference, peripheral resistance) and explain the following term: mean arterial pressure.
#+date: [2021-10-16 Sat]
#+hugo_tags: medschool learning_objective
