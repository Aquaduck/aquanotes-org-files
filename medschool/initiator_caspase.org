#+title: Initiator caspase
#+date: [2021-09-26 Sun]
#+hugo_tags: medschool concept

* Concept links
- A class of [[file:caspase.org][caspases]]
- main function is to activate [[file:executioner_caspase.org][executioner caspases]] via cleavage of inactive executioner caspase dimers
