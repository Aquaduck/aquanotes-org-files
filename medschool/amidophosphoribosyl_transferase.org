#+title: Amidophosphoribosyl transferase
#+date: [2021-10-06 Wed]
#+hugo_tags: medschool concept
#+roam_alias: ATase

* Concept links
- Catalyzes the first step in [[file:purine.org][purine]] biosynthesis
