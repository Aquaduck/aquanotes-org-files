#+title: List the five major mechanisms by which intermediary metabolism is regulated
#+date: [2021-08-29 Sun]
#+hugo_tags: medschool learning_objective
#+roam_alias: "Describe the five common mechanisms regulating intermediary metabolism and give examples of their use in glucose catabolism."
From [[file:intermediary_metabolism_and_its_regulation.org][Intermediary Metabolism and its Regulation]]
* Summary of major regulators of intermediary metabolism
#+caption: The common mechanisms by which enzyme activity is regulated
| Regulator event                    | Typical effector      | Results                          | Time required for change |
|------------------------------------+-----------------------+----------------------------------+--------------------------|
| Substrate availability             | Substrate             | Change in v_{o}                  | Immediate                |
| Product inhibition                 | Reaction product      | Change in V_{max} and/or K_{m}   | Immediate                |
| Allosteric control                 | Pathway end product   | Change in V_{max} and/or K_{0.5} | Immediate                |
| Covalent modification              | Another enzyme        | Change in V_{max} and/or K_{m}   | Immediate to minutes     |
| Synthesis or degradation of enzyme | Hormone or metabolite | Change in amount of enzyme       | Hours to days            |

Note: First two mechanisms detailed in JumpStart, Allosteric control covered in [[file:kinetic_and_allosteric_regulation_of_glucose_catabolism.org][Kinetic and Allosteric Regulation of Glucose Regulation]]
* Examples in glucose catabolism
** Substrate availability
- [[file:hexokinase.org][Hexokinase]] needs glucose
** Product inhibition
- [[file:hexokinase.org][Hexokinase]] inhibited by [[file:glucose_6_phosphate.org][G6P]]
  + Note: *[[file:glucokinase.org][glucokinase]] not subject to same regulation*
** Allosteric control
- [[file:6_phosphofructo_1_kinase.org][PFK-1]]
  + Inhibited by [[file:citrate.org][citrate]]
  + Regulated by energy charge
- [[file:pyruvate_kinase.org][Pyruvate kinase]]
  + Activated by fructose-1,6-bisphosphate
  + Inhibited by:
    - Alanine
    - Long-chain fatty acids
  + Regulated by energy charge
** Covalent modification
- [[file:pyruvate_kinase.org][Pyruvate kinase]]
  + Downregulated by phosphorylation by PKA
- [[file:pfk_2.org][PFK-2]]
  + /Kinase component/ downregulated by phosphorylation by PKA -> increase activity of /phosphatase/ component
** Synthesis or degradation of enzyme
- Insulin stimulates glycolysis
- Glucagon inhibits glycolysis
