#+title: Right ventricular outflow tract obstruction
#+date: [2021-10-26 Tue]
#+hugo_tags: medschool concept patho
#+roam_alias: RVOTO

* Concept links
- Inability of blood to pass out of the [[file:right_ventricle.org][right ventricle]] through the [[file:pulmonary_valve.org][pulmonic valve]].

