#+title: Chylomicron retention disease
#+date: [2021-09-13 Mon]
#+hugo_tags: medschool learning_objective concept
#+roam_alias: "Anderson disease"

* From [[file:molecular_analysis_and_intestinal_expression_of_sar1_genes_and_proteins_in_anderson_s_disease_chylomicron_retention_disease.org][Molecular analysis and intestinal expression of SAR1 genes and proteins in Anderson's disease (Chylomicron retention disease)]]
- Results from a mutation in the [[file:sar1b_gene.org][SAR1B gene]] which encodes the [[file:sar1b_protein.org][SAR1B protein]]
  + A small GTPase involved [[file:cop_ii.org][COPII]]-dependent transport of proteins from the [[file:endoplasmic_reticulum.org][ER]] to the [[file:golgi_apparatus.org][Golgi]]
    - Sar1/COPII protein complex required for fusion of the specific chylomicron transport vesicle /Pre-chylomicron transport vesicle (PCTV)/ with the Golgi
    - The mutation here prevents this vesicle from fusing with the Golgi, resulting in the chylomicrons being sent back to the ER and collecting there
