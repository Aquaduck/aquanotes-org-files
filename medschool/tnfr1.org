#+title: TNFR1
#+date: [2021-09-28 Tue]
#+hugo_tags: medschool concept

* Concept links
- The main [[file:tumor_necrosis_factor_receptor.org][TNFR]] that transduces signals from [[file:tumor_necrosis_factor.org][TNF-α]]
  + Regular function is to induce [[file:nfκb.org][NFκB]] and promote cell survival and growth
    - *Only* when this pathway is disrupted does it induce cell death
- Signalling to [[file:tnfr1_associated_death_domain_protein.org][TRADD]] is cell-type specific
