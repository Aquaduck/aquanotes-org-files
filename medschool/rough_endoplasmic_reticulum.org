#+title: Rough endoplasmic reticulum
#+date: [2021-08-01 Sun]
#+hugo_tags: medschool concept
#+roam_alias: RER
* Overview
- An [[file:organelle.org][organelle]]
- A type of [[file:endoplasmic_reticulum.org][Endoplasmic reticulum]]
