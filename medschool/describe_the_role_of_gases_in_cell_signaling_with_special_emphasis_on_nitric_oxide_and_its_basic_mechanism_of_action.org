#+title: Describe the role of gases in cell signaling with special emphasis on nitric oxide and its basic mechanism of action.
#+date: [2021-09-04 Sat]
#+hugo_tags: medschool learning_objective
* From [[file:cell_signalling_ii_intracellular_signal_pathways.org][Cell Signalling II - Intracellular Signal Pathways]]
** Gases as second messengers - [[file:nitric_oxide.org][NO]]
- NO is synthesized by certain cells (e.g. epithelial) from arginine
- Three (of many) physiological responses to elevated levels of NO:
  1) Smooth muscle relaxation
  2) Neurotransmission in CNS
  3) Cell-mediated immune response
*** Mechanism of action
1) Signal binds to receptor
2) Receptor activates [[file:nitric_oxide_synthase.org][NO synthase]] -> synthesizes NO
3) NO diffuses to neighboring cells across membranes
4) NO binds to /guanylyl cyclase/ -> activation of cGMP pathway
5) Triggers smooth muscle relaxation
** [[file:carbon_monoxide.org][CO]]
- CO is a physiologically important signal molecule and is thought to work in the same way as nitric oxide
