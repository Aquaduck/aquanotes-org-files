#+title: Population mean
#+date: [2021-10-31 Sun]
#+hugo_tags: medschool concept

* Concept links
- [[file:mean.org][Mean]] of a [[file:population.org][Population]]
