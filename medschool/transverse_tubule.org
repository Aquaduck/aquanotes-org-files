#+title: Transverse tubule
#+date: [2021-09-13 Mon]
#+hugo_tags: medschool concept
#+roam_alias: "T tubule"

* Concept links
- Invaginations formed from [[file:sarcolemma.org][sarcolemma]] of skeletal muscle cells which extend into the cell
