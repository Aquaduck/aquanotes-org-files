#+title: Shunt
#+date: [2021-10-26 Tue]
#+hugo_tags: medschool concept patho

* Concept links
- Pathological connections between the right and left [[file:heart.org][Heart]] chambers
- Results in [[file:cyanosis.org][cyanosis]]
