#+title: Bowman capsule
#+date: [2021-10-23 Sat]
#+hugo_tags: medschool concept
#+roam_alias: "Bowman's capsule"

* Concept links
- Encloses the [[file:glomerulus.org][Glomerulus]] within the [[file:renal_corpuscle.org][Renal corpuscle]]

