#+title: Stratum basale
#+date: [2021-09-21 Tue]
#+hugo_tags: medschool concept
#+roam_alias: "Stratum germinativum" "Basal layer"

* Concept links
- The lowest layer of the [[file:epidermis.org][epidermis]] where cell division occurs

