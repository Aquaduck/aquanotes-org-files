#+title: Name and describe five basic epithelial types and relationship to the basement membrane.
#+date: [2021-09-13 Mon]
#+hugo_tags: medschool learning_objective

* From [[file:brs_cell_biology_histology.org][BRS Cell Biology & Histology]]
** Classification of epithelia table (p. 68)
| Type                                | Shape of Superficial Layer          | Typical Locations                                            |
| Simple squamous                     | Flattened                           | Endothelium (lining of blood vessels)                        |
|                                     |                                     | Mesothelium (lining of peritonium and pleura)                |
| Simple cuboidal                     | Cuboidal                            | Lining of distal tubule in kidneys                           |
|                                     |                                     | Lining of ducts in some glands                               |
|                                     |                                     | Surface of ovary                                             |
| Simple columnar                     | Columnar                            | Lining of intestine                                          |
|                                     |                                     | Lining of stomach                                            |
|                                     |                                     | Lining of excretory ducts in some glands                     |
| Pseudostratified                    | All cells rest on basal lamina      | Lining of trachea                                            |
|                                     | Not all reach the lumen             | Lining of primary bronchi                                    |
|                                     | Causes appearance of stratification | Lining of nasal cavity                                       |
|                                     |                                     | Lining of excretory ducts in some glands                     |
| Stratified squamous (nonkeratinzed) | Flattened (nucleated)               | Lining of esophagus                                          |
|                                     |                                     | Lining of vagina                                             |
|                                     |                                     | Lining of mouth                                              |
|                                     |                                     | Lining of true vocal cords                                   |
| Stratified squamous (keratinized)   | Flattened (without nuclei)          | Epidermis of skin                                            |
| Stratified cuboidal                 | Cuboidal                            | Lining of ducts in sweat glands                              |
| Stratified columnar                 | Columnar                            | Lining of large excretory ducts in some glands               |
|                                     |                                     | Lining of cavernous urethra                                  |
| Transitional                        | Dome-shaped (when relaxed)          | Lining of urinary passages from renal calyces to the urethra |
|                                     | Flattened (when stretched)          |                                                              |
** Classification of epithelia diagrams (p.68)
:PROPERTIES:
:ID:       67bd7255-b292-4796-b488-b5493ab63982
:END:
#+caption: Classifications of epithelia

[[attachment:_20210913_213055screenshot.png]]
