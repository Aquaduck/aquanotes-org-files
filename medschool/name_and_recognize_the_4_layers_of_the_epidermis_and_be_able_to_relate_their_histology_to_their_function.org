#+title: Name and recognize the 4 layers of the epidermis and be able to relate their histology to their function.
#+date: [2021-09-21 Tue]
#+hugo_tags: medschool learning_objective

* From [[file:histology_skin_pre_session_powerpoint.org][Histology - Skin Pre-session Powerpoint]]
** [[file:epidermis.org][Epidermis]] - strata
*** [[file:stratum_basale.org][Stratum basale]]
- Single layer of *cuboidal cells* attached to the basement membrane
- New cells are generated from this layer (stem cells)
*** [[file:stratum_spinosum.org][Stratum spinosum]] - "spiny layer"
- Named for the spiny cell-to-cell bridges that are sometimes visible
  + Correspond to [[file:desmosome.org][desmosomes]]
*** [[file:stratum_granulosum.org][Stratum granulosum]] - granular layer
- [[file:keratinocyte.org][Keratinocytes]] start to degenerate and make keratin precursors
  + Keratin-precursors accumulate in the cytoplasm as /keratin-hyaline granules/
*** [[file:stratum_corneum.org][Stratum corneum]] - keratin layer
- Skeletal remains of dead cells that are still held together by desmosomes
- [[file:keratin.org][Keratin]] and thick plasma membranes form a waxy barrier
