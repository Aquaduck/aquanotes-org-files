#+title: throttled
#+date: [2021-07-30 Fri]
#+hugo_tags: fedora
[[https://github.com/erpalma/throttled][Github]]
* Overview
- Throttled is a fix for certain thinkpad laptops running under linux whose CPUs are in a permanently throttled state
