#+title: Vim
#+date: [2021-07-07 Wed]
:PROPERTIES:
:END:
* *Overview*
- *Vim* is a terminal-based text editor that uses [[file:modal_editing.org][Modal Editing]]
* Comparison to Kakoune
- I currently use Vim over [[file:Kakoune.org][Kakoune]] due to familiarity
